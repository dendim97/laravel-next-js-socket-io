// Import module 'socket.io-client'
import { io, Socket } from 'socket.io-client';


// Hanya membuat koneksi Socket.IO di sisi klien (client-side)
const isBrowser = typeof window !== 'undefined';
export const socket: Socket = isBrowser ? io('http://localhost:2000') : undefined;